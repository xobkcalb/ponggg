#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division, print_function
import sys

from twisted.spread import pb
from twisted.internet import reactor, stdio
from twisted.protocols import basic


def on_error(error):
    print('Error:', error, file=sys.stderr)
    reactor.stop()


class Chatter(basic.LineReceiver):
    def __init__(self, client):
        self.client = client
        self.delimiter = '\n'

    def connectionMade(self):
        self.transport.write('>>> ')

    def lineReceived(self, line):
        self.client.sendMessage(line)


class Client(pb.Referenceable):
    def __init__(self):
        self.client_ref = None
        self.chatter = None

    def connected(self, client_ref):
        self.client_ref = client_ref
        self.chatter = Chatter(self)
        stdio.StandardIO(self.chatter)

    def sendMessage(self, message):
        try:
            self.client_ref.callRemote('sendMessage', message).addErrback(on_error)
        except pb.DeadReferenceError, error:
            on_error(error)

    def remote_receiveMessage(self, user_name, message):
        self.chatter.transport.write('[%s]: %s\n>>> ' % (user_name, message))


def main(server_ref):
    client = Client()
    server_ref.callRemote('login', 'client 1', client).addCallback(client.connected)

if __name__ == '__main__':
    factory = pb.PBClientFactory()
    reactor.connectTCP('localhost', 8123, factory)
    deferred_server = factory.getRootObject()
    deferred_server.addCallback(main)
    deferred_server.addErrback(on_error)
    reactor.run()
