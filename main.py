#!/usr/bin/python
# -*- coding: utf-8 -*-
# ping_ping_multi.py v0.1
# by ITJunky and Zu

from __future__ import division, print_function
import logging
import pygame

from debug import DebugInfo
from events import EventManager
from frame import FrameTicker
from input import KeyboardHandler
from paddle import Paddle
from physics import PhysicsSimulator
from physics.objects import WorldBoundsRect
from render import SimpleRenderer
from render.textures import init_texlib
from util import Vec2d

WIDTH, HEIGHT = 1200, 700  # Размеры экрана


def init():
    """Инициализация программы"""
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

    pygame.init()
    pygame.display.set_caption('Ping-Pong Multi')
    pygame.display.set_mode((WIDTH, HEIGHT))

    init_texlib('data/textures')


def main():
    from balls import create_balls
    from gui import create_test_screen
    from default_binding import register_keyboard_binding

    init()

    event_manager = EventManager()

    '''Порядок создания листенеров это порядок их вызова для обработки евента'''
    ticker = FrameTicker(event_manager)
    KeyboardHandler(event_manager)
    register_keyboard_binding(event_manager)
    physics_simulator = PhysicsSimulator(event_manager)
    renderer = SimpleRenderer(event_manager)
    debug_info = DebugInfo(event_manager)

    screen = create_test_screen(event_manager)
    renderer.add_objects([screen])

    screen_center = Vec2d(WIDTH / 2, HEIGHT / 2)
    world_bounds = WorldBoundsRect(screen_center, screen_center - Vec2d(100, 50))
    physics_simulator.add_objects([world_bounds])

    objects = create_balls(10, pygame.Rect(150, 100, WIDTH - 300, HEIGHT - 400))
    physics_simulator.add_objects(objects)
    renderer.add_objects(objects)

    paddle = Paddle(screen_center, event_manager)
    renderer.add_objects([paddle])
    physics_simulator.add_objects([paddle])

    renderer.add_objects([debug_info])

    ticker.run()

if __name__ == '__main__':
    main()
