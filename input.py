# -*- coding: utf-8 -*-

from __future__ import division, print_function
import pygame

from events import EventListenerBase
from event_classes import FrameStartEvent, ExitGameEvent, KeyboardEvent


class StatefulKeys:
    def __init__(self):
        self.keys = []
        self.changes = {}

    def update_keys(self, keys):
        if self.keys:
            self.changes.clear()
            for key in range(pygame.K_FIRST, pygame.K_LAST):
                if keys[key] != self.keys[key]:
                    self.changes[key] = keys[key]
        self.keys = keys

    def is_pressed(self, key):
        return self.changes.get(key)

    def is_released(self, key):
        return key in self.changes and not self.changes.get(key)


class KeyboardHandler(EventListenerBase):
    def __init__(self, event_manager):
        EventListenerBase.__init__(self, event_manager)
        self.stateful_keys = StatefulKeys()

    """Синглетон, который обрабатывает инпут"""
    def process_event(self, event):
        if isinstance(event, FrameStartEvent):
            for event in pygame.event.get():
                # Выход по крестику
                if event.type == pygame.QUIT:
                    self.event_manager.post_event(ExitGameEvent())

            self.__process_keys()

    def __process_keys(self):
        """Повторные события при удерживании клавиши работают криво, поэтому мы делаем это вручную"""
        self.stateful_keys.update_keys(pygame.key.get_pressed())
        for key, is_pressed in self.stateful_keys.changes.iteritems():
            event_type = KeyboardEvent.KEY_PRESSED if is_pressed else KeyboardEvent.KEY_RELEASED
            self.event_manager.post_event(KeyboardEvent(event_type, key))


class Keybind:
    def __init__(self, event_type, event_key, event_creator, *creator_args):
        """
        :type event_type: int
        :type event_key: int
        """
        self.event_type = event_type
        self.event_key = event_key
        self.event_creator = event_creator
        self.creator_args = creator_args


class KeyboardBinding(EventListenerBase):
    """Биндинг клавиатурных евентов. НЕ регистрируется автоматически в евент менеджере."""
    def __init__(self, event_manager, keybind_list):
        """
        :type event_manager: EventManager
        :type keybind_list: collections.Iterable[Keybind]
        """
        EventListenerBase.__init__(self, event_manager, False)
        self.keybind_map = {}
        """dict[tuple(int, int), Keybind]]"""

        for keybind in keybind_list:
            self.keybind_map[(keybind.event_type, keybind.event_key)] = keybind

    def process_event(self, event):
        if isinstance(event, KeyboardEvent):
            keybind = self.keybind_map.get((event.event_type, event.event_key))
            if not keybind:
                return
            self.event_manager.post_event(keybind.event_creator(*keybind.creator_args))

