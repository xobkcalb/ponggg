# -*- coding: utf-8 -*-

from __future__ import division, print_function
import logging
from twisted.spread import pb
from twisted.internet import reactor

from debug import DebugInfo
from events import EventManager
from frame import TwistedTicker
from paddle import ServerPaddle
from physics import PhysicsSimulator
from physics.objects import WorldBoundsRect
from render import LoggingRenderer
from util import Vec2d

WIDTH, HEIGHT = 1200, 700  # Размеры экрана


def init():
    """Инициализация программы"""
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.INFO)


def main():
    init()

    event_manager = EventManager()

    '''Порядок создания листенеров это порядок их вызова для обработки евента'''
    TwistedTicker(event_manager)
    physics_simulator = PhysicsSimulator(event_manager)
    renderer = LoggingRenderer(event_manager, logging.INFO)
    debug_info = DebugInfo(event_manager)

    screen_center = Vec2d(WIDTH / 2, HEIGHT / 2)
    world_bounds = WorldBoundsRect(screen_center, screen_center - Vec2d(50, 50))
    physics_simulator.add_objects([world_bounds])

    renderer.add_objects([debug_info])

    # noinspection PyClassHasNoInit
    class GameServer(pb.Root):
        @staticmethod
        def remote_connect(paddle_ref):
            paddle = ServerPaddle(screen_center, paddle_ref)
            physics_simulator.add_objects([paddle])

            return paddle

    server = GameServer()
    reactor.listenTCP(8321, pb.PBServerFactory(server))
    reactor.run()

if __name__ == '__main__':
    main()
