# -*- coding: utf-8 -*-

from __future__ import division, print_function
import pygame
from event_classes import KeyboardEvent, MenuActionOKEvent, MenuActionCancelEvent, MenuFocusNextEvent, \
    MenuFocusPrevEvent, MenuActionShowMenu, ExitGameEvent, FlipGravity
from events import EventListenerBase
from input import KeyboardBinding, Keybind

from render import DrawableObject
from util import Vec2d


# noinspection PyAbstractClass
class Midget(DrawableObject):
    """Базовый класс всех элементов интерфейса"""

    def __init__(self, uid, pos):
        """
        :type uid: str
        :type pos: util.Vec2d
        """
        self.uid = uid
        self.pos = pos
        self.parent = None

    def get_global_pos(self):
        """
        :rtype : util.Vec2d
        """
        global_pos = self.pos
        parent = self.parent
        while parent is not None:
            global_pos += parent.pos
            parent = parent.parent
        return global_pos


class TextMidget(Midget):
    def __init__(self, uid, pos, color, text):
        """
        :type uid: str
        :type pos: Vec2d
        :type color: (int, int, int)
        :type text: str
        """
        Midget.__init__(self, uid, pos)
        self.color = color
        self.text = text

    def render(self, renderer):
        """
        :type renderer: render.RendererBase
        """
        renderer.draw_text(self.color, self.get_global_pos(), self.text)


class ImageMidget(Midget):
    def __init__(self, uid, pos, size, tex_name):
        """
        :type uid: str
        :type pos: Vec2d
        :type size: util.Vec2d
        :type tex_name: str
        """
        Midget.__init__(self, uid, pos)
        self.size = size
        self.tex_name = tex_name

    def render(self, renderer):
        """
        :type renderer: render.RendererBase
        """
        renderer.draw_tex_rect(self.get_global_pos(), self.size, self.tex_name)


class ContainerMidget(Midget):
    def __init__(self, uid, pos, children):
        """
        :type uid: str
        :type pos: util.Vec2d
        :type children: collections.Iterable[Midget]
        """
        Midget.__init__(self, uid, pos)
        self.children = []
        """:type : list[Midget]"""

        for child in children:
            self._add_child(child)

    def _add_child(self, midget):
        """
        :type midget: Midget
        """
        self.children.append(midget)
        midget.parent = self

    def render(self, renderer):
        """
        :type renderer: render.RendererBase
        """
        for child in self.children:
            child.render(renderer)


class StatefulContainerMidget(Midget):
    def __init__(self, uid, pos, states_map):
        """
        :type uid: str
        :type pos: util.Vec2d
        :type states_map: dict[str, Midget]
        """
        Midget.__init__(self, uid, pos)
        self.states_map = states_map.copy()
        for midget in self.states_map.itervalues():
            midget.parent = self
        self.active_state = None

    def set_state(self, state):
        """
        :type state: str | None
        """
        if state not in self.states_map:
            state = None
        self.active_state = state

    def render(self, renderer):
        """
        :type renderer: render.RendererBase
        """
        if self.active_state is not None:
            self.states_map[self.active_state].render(renderer)


# noinspection PyAbstractClass
class FocusableMidget(Midget, EventListenerBase):
    def __init__(self, uid, pos, event_manager):
        """
        :type uid: str
        :type pos: util.Vec2d
        :type event_manager: events.EventManager
        """
        Midget.__init__(self, uid, pos)
        EventListenerBase.__init__(self, event_manager, False)

    def focus_received(self):
        raise NotImplementedError()

    def focus_lost(self):
        raise NotImplementedError()


# noinspection PyAbstractClass
class FocusableContainerMidget(ContainerMidget, EventListenerBase):
    def __init__(self, uid, pos, event_manager, children):
        """
        :type uid: str
        :type pos: util.Vec2d
        :type event_manager: events.EventManager
        :type children: collections.Iterable[Midget]
        """
        ContainerMidget.__init__(self, uid, pos, children)
        EventListenerBase.__init__(self, event_manager, False)
        self.focusable_children = [child for child in self.children if isinstance(child, FocusableMidget)]
        """:type : list[FocusableMidget]"""
        self.focused_index = 0
        if self.focusable_children:
            self._focused().focus_received()

    def _focused(self):
        return self.focusable_children[self.focused_index]

    def process_event(self, event):
        if not self.focusable_children:
            return

        if isinstance(event, MenuFocusNextEvent):
            if self.focused_index < len(self.focusable_children) - 1:
                self._focused().focus_lost()
                self.focused_index += 1
                self._focused().focus_received()
        elif isinstance(event, MenuFocusPrevEvent):
            if self.focused_index > 0:
                self._focused().focus_lost()
                self.focused_index -= 1
                self._focused().focus_received()
        else:
            self._focused().process_event(event)


# noinspection PyAbstractClass
class ScreenMidget(FocusableContainerMidget):
    def __init__(self, uid, pos, event_manager, grab_keyboard, children):
        """
        :type uid: str
        :type pos: util.Vec2d
        :type event_manager: events.EventManager
        :type grab_keyboard: bool
        :type children: collections.Iterable[Midget]
        """
        FocusableContainerMidget.__init__(self, uid, pos, event_manager, children)
        self.keyboard_grabber = _create_menu_binding(self.event_manager) if grab_keyboard else None


# noinspection PyAbstractClass
class ScreenMidgetStack(ContainerMidget, EventListenerBase):
    def __init__(self, uid, pos, bottom_screen, event_manager):
        """
        :type uid: str
        :type pos: util.Vec2d
        :type bottom_screen: ScreenMidget
        :type event_manager: events.EventManager
        """
        ContainerMidget.__init__(self, uid, pos, ())
        EventListenerBase.__init__(self, event_manager)
        self.push_screen(bottom_screen)

    def push_screen(self, screen):
        """
        :type screen: ScreenMidget
        """
        ContainerMidget._add_child(self, screen)
        self.event_manager.register_listener(screen)
        if screen.keyboard_grabber:
            self.event_manager.grab_event(screen.keyboard_grabber, KeyboardEvent)

    def pop_screen(self):
        if len(self.children) <= 1:
            return
        screen = self.children.pop()
        """:type : ScreenMidget"""

        screen.parent = None
        self.event_manager.unregister_listener(screen)
        if screen.keyboard_grabber:
            self.event_manager.release_grab(screen.keyboard_grabber)

    def process_event(self, event):
        if isinstance(event, MenuActionShowMenu):
            menu_screen = _create_menu_screen(self.event_manager)
            self.push_screen(menu_screen)
        elif isinstance(event, MenuActionCancelEvent):
            self.pop_screen()


class ButtonMidget(FocusableMidget):
    STATE_FOCUSED = 'focused'
    STATE_UNFOCUSED = 'unfocused'

    def __init__(self, uid, pos, event_manager, focused_midget, unfocused_midget, event_creator, *event_args):
        """
        :type uid: str
        :type pos: util.Vec2d
        :type event_manager: events.EventManager
        :type focused_midget: Midget
        :type unfocused_midget: Midget
        """
        FocusableMidget.__init__(self, uid, pos, event_manager)
        self.delegate_midget = StatefulContainerMidget(uid+'_inner', Vec2d(), {
            self.STATE_FOCUSED: focused_midget,
            self.STATE_UNFOCUSED: unfocused_midget
        })
        self.delegate_midget.set_state(self.STATE_UNFOCUSED)
        self.delegate_midget.parent = self
        self.event_creator = event_creator
        self.creator_args = event_args

    def focus_received(self):
        self.delegate_midget.set_state(self.STATE_FOCUSED)

    def focus_lost(self):
        self.delegate_midget.set_state(self.STATE_UNFOCUSED)

    def process_event(self, event):
        if isinstance(event, MenuActionOKEvent):
            self.event_manager.post_event(self.event_creator(*self.creator_args))

    def render(self, renderer):
        self.delegate_midget.render(renderer)


def _create_menu_binding(event_manager):
    """
    :type event_manager: events.EventManager
    """
    binding = KeyboardBinding(event_manager, [
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_RETURN, MenuActionOKEvent),
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_ESCAPE, MenuActionCancelEvent),
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_DOWN, MenuFocusNextEvent),
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_UP, MenuFocusPrevEvent),
    ])
    return binding


def _create_menu_screen(event_manager):
    """адовый хардкод, который создает внутриигровое меню
    :type event_manager: events.EventManager
    :rtype : ScreenMidget
    """
    menu_screen = ScreenMidget('menu', Vec2d(400, 200), event_manager, True, [
        ButtonMidget(
            'cancel button', Vec2d(50, 50), event_manager,
            TextMidget('cancel label focused', Vec2d(), (0, 255, 255), 'CLOSE MENU'),
            TextMidget('cancel label unfocused', Vec2d(), (0, 0, 255), 'CLOSE MENU'),
            MenuActionCancelEvent),
        ButtonMidget(
            'flip button', Vec2d(50, 100), event_manager,
            TextMidget('flip label focused', Vec2d(), (0, 255, 255), 'FLIP GRAVITY'),
            TextMidget('flip label unfocused', Vec2d(), (0, 0, 255), 'FLIP GRAVITY'),
            FlipGravity),
        ButtonMidget(
            'exit button', Vec2d(50, 150), event_manager,
            TextMidget('exit label focused', Vec2d(), (0, 255, 255), 'EXIT GAME'),
            TextMidget('exit label unfocused', Vec2d(), (0, 0, 255), 'EXIT GAME'),
            ExitGameEvent),
    ])
    return menu_screen


def create_test_screen(event_manager):
    """адовый хардкод, который типа создает HUD
    :type event_manager: events.EventManager
    :rtype : ScreenMidgetStack
    """
    up_line = ContainerMidget(
        'up_line', Vec2d(0, 20),
        [ImageMidget('brick_u%02d' % i, Vec2d(75*i, 0), Vec2d(75, 30), 'gray_brick') for i in range(16)]
    )
    down_line = ContainerMidget(
        'down_line', Vec2d(0, 650),
        [ImageMidget('brick_d%02d' % i, Vec2d(75*i, 0), Vec2d(75, 30), 'gray_brick') for i in range(16)]
    )
    left_line = ContainerMidget(
        'left_line', Vec2d(25, 50),
        [ImageMidget('brick_l%02d' % i, Vec2d(0, 30*i), Vec2d(75, 30), 'gray_brick') for i in range(20)]
    )
    right_line = ContainerMidget(
        'right_line', Vec2d(1100, 50),
        [ImageMidget('brick_l%02d' % i, Vec2d(0, 30*i), Vec2d(75, 30), 'gray_brick') for i in range(20)]
    )
    background = ScreenMidget('background', Vec2d(), event_manager, False, [up_line, down_line, left_line, right_line])
    return ScreenMidgetStack('root', Vec2d(), background, event_manager)
