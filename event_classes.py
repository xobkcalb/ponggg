# -*- coding: utf-8 -*-

from __future__ import division, print_function


class FrameStartEvent:
    """Евент начала фрейма"""
    def __init__(self, elapsed):
        """
        :type elapsed: float
        """
        self.elapsed = elapsed


class KeyboardEvent:
    KEY_PRESSED = 1
    KEY_RELEASED = 2

    def __init__(self, event_type, event_key):
        """
        :type event_type: int
        :type event_key: int
        """
        self.event_type = event_type
        self.event_key = event_key


# noinspection PyClassHasNoInit
class ExitGameEvent:
    """Евент выхода из игры"""
    pass


# noinspection PyClassHasNoInit
class PaddleAccelerateLeft:
    """Игрок пытается сдвинуть биту влево"""
    def __init__(self, active):
        self.active = active


# noinspection PyClassHasNoInit
class PaddleAccelerateRight:
    """Игрок пытается сдвинуть биту вправо"""
    def __init__(self, active):
        self.active = active


# noinspection PyClassHasNoInit
class FlipGravity:
    """Инверсия силы тяжести"""
    pass


class FrameInfoEvent:
    """Евент для отображения численной дебажной информации (например, FPS или время кадра)"""
    def __init__(self, text, value):
        """
        :type text: str
        :type value: object
        """
        self.text = text
        self.value = value


# noinspection PyClassHasNoInit
class DebugElapsedSlowdown:
    pass


# noinspection PyClassHasNoInit
class DebugElapsedSpeedup:
    pass


# noinspection PyClassHasNoInit
class MenuActionShowMenu:
    pass


# noinspection PyClassHasNoInit
class MenuActionOKEvent:
    pass


# noinspection PyClassHasNoInit
class MenuActionCancelEvent:
    pass


# noinspection PyClassHasNoInit
class MenuFocusNextEvent:
    pass


# noinspection PyClassHasNoInit
class MenuFocusPrevEvent:
    pass
