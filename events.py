# -*- coding: utf-8 -*-

from __future__ import division, print_function


class _GrabInfo:
    def __init__(self, listener, event_classes):
        """
        :type listener: EventListenerBase
        :type event_classes: collections.Iterable
        """
        self.listener = listener
        self.event_classes = event_classes


class EventManager:
    """Синглетон, который раздает евенты листенерам; каждый листенер получает каждый евент"""
    def __init__(self):
        self.listeners = []
        """:type : list[EventListenerBase]"""
        self.event_grabs = []
        """:type : list[_GrabInfo]"""

    def grab_event(self, listener, *event_classes):
        """Захватывает все евенты указанных классов для передачи только одному листенеру
        :type listener: EventListenerBase
        """
        if not event_classes:
            return
        self.event_grabs.append(_GrabInfo(listener, event_classes))

    def release_grab(self, listener):
        """Отпускает все захваты евентов для данного листенера
        :type listener: EventListenerBase
        """
        self.event_grabs[:] = [grab for grab in self.event_grabs if grab.listener != listener]

    def post_event(self, event):
        """Если для класса данного евента включен захват для определенного листенера (или нескольких листенеров) --
        вызывает ``process_event`` у последнего захватившего листенера, иначе вызывает ``process_event`` каждого
        листенера в том порядке в котором листенеры регистрировались"""
        grab_order = reversed(self.event_grabs)
        """:type : collections.Iterable[_GrabInfo]"""
        for grab in grab_order:
            if isinstance(event, grab.event_classes):
                grab.listener.process_event(event)
                return

        for listener in self.listeners:
            listener.process_event(event)

    def register_listener(self, listener):
        """Регистрация листенеров
        :type listener: EventListenerBase
        """
        self.listeners.append(listener)

    def unregister_listener(self, listener):
        """Дерегистрация листенеров
        :type listener: EventListenerBase
        """
        self.listeners[:] = [l for l in self.listeners if l != listener]


class EventListenerBase:
    """Удобный базовый класс для листенеров с автоматической регистрацией в менеджере"""
    def __init__(self, event_manager, auto_register=True):
        """
        :type event_manager: EventManager
        """
        self.event_manager = event_manager
        if auto_register:
            self.event_manager.register_listener(self)

    def process_event(self, event):
        raise NotImplementedError()
