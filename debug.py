# -*- coding: utf-8 -*-

from __future__ import division, print_function
from event_classes import FrameInfoEvent, FlipGravity
from events import EventListenerBase
from render import DrawableObject
from util import Vec2d


LINE_HEIGHT = 20
TEXT_COLOR = 100, 250, 20


class DebugInfo(EventListenerBase, DrawableObject):
    def __init__(self, event_manager, pos=Vec2d(50, 20)):
        """
        :type event_manager: events.EventManager
        """
        EventListenerBase.__init__(self, event_manager)
        self.pos = pos
        self.frame_info = []
        self.text_color = TEXT_COLOR

    def render(self, renderer):
        """
        :type renderer: render.RendererBase
        """
        pos = self.pos
        for info in self.frame_info:
            text = '%s: %s' % (info.text, info.value)
            renderer.draw_text(self.text_color, pos, text)
            pos += Vec2d(0, LINE_HEIGHT)
        self.frame_info[:] = []

    def process_event(self, event):
        if isinstance(event, FlipGravity):
            self.text_color = tuple(255 - c for c in self.text_color)
        elif isinstance(event, FrameInfoEvent):
            self.frame_info.append(event)
