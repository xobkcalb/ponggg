#!/usr/bin/python
# -*- coding: utf-8 -*-

from __future__ import division, print_function

from twisted.spread import pb
from twisted.internet import reactor


class User(pb.Referenceable):
    def __init__(self, name, server, remote_client):
        self.name = name
        self.server = server
        self.remote_client = remote_client

    def remote_sendMessage(self, message):
        self.server.send(self.name, message)

    def receiveMessage(self, user_name, message):
        self.remote_client.callRemote('receiveMessage', user_name, message)


class ChatServer(pb.Root):
    def __init__(self):
        self.users = {}

    def remote_login(self, username, remote_client):
        user = self.users.get(username)
        if user:
            user.remote_client.broker.transport.loseConnection()
        user = User(username, self, remote_client)
        self.users[username] = user
        print('<client "%s" connected>' % username)
        remote_client.notifyOnDisconnect(lambda remote: self.remote_disconnect(username, remote))
        return user

    def remote_disconnect(self, username, remote):
        if self.users.get(username) == remote:
            del self.users[username]
        print('<client "%s" disconnected>' % username)

    def send(self, user_name, message):
        print('[%s]: %s' % (user_name, message))
        for user in self.users.itervalues():
            user.receiveMessage(user_name, message)


if __name__ == '__main__':
    server = ChatServer()
    reactor.listenTCP(8123, pb.PBServerFactory(server))
    reactor.run()
