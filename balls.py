# -*- coding: utf-8 -*-

from __future__ import division, print_function
import random
from physics.objects import CollidingCircle
from render import DrawableObject
from util import Vec2d

BALL_COLOR = 250, 240, 30  # Цвет шара
RADIUS = 40  # Радиус шара


class Ball(DrawableObject, CollidingCircle):
    def __init__(self, pos, radius, color):
        """
        :type pos: Vec2d
        :type radius: int
        :type color: (int, int, int)
        """
        CollidingCircle.__init__(self, pos, Vec2d(), Vec2d(), radius)
        self.color = color

    def render(self, renderer):
        """Рисует шар в его текущей позиции
        :type renderer: render.RendererBase
        """
        renderer.draw_circle(self.color, self.pos, self.radius)


def create_balls(count, rect):
    """
    :type count: int
    :type rect: pygame.Rect
    """
    result = []
    for _ in range(count):
        x = random.randint(rect.left, rect.right)
        y = random.randint(rect.top, rect.bottom)
        pos = Vec2d(x, y)
        ball = Ball(pos, RADIUS, BALL_COLOR)
        result.append(ball)
    return result
