# -*- coding: utf-8 -*-

from __future__ import division, print_function
from twisted.spread import pb

from event_classes import PaddleAccelerateLeft, PaddleAccelerateRight
from events import EventListenerBase
from physics.objects import CollidingRect, CollidingObject
from render import DrawableObject
from util import Vec2d

PADDLE_COLOR = 250, 30, 50  # Цвет биты
PADDLE_SIZE = Vec2d(50, 20)
PADDLE_EXTENTS = PADDLE_SIZE/2
PADDLE_ACCEL = Vec2d(x=0.003)


class Paddle(EventListenerBase, DrawableObject, CollidingRect):
    """Бита"""
    def __init__(self, pos, event_manager):
        """
        :type pos: Vec2d
        :type event_manager: events.EventManager
        """
        EventListenerBase.__init__(self, event_manager)
        CollidingRect.__init__(self, pos, Vec2d(), Vec2d(), PADDLE_EXTENTS)

    def render(self, renderer):
        """Рисует биту в ее текущей позиции
        :type renderer: render.RendererBase
        """
        renderer.draw_tex_rect(self.pos - self.extents, self.extents*2, 'paddle')

    def process_event(self, event):
        if isinstance(event, PaddleAccelerateLeft):
            self.accel += -PADDLE_ACCEL if event.active else PADDLE_ACCEL
        elif isinstance(event, PaddleAccelerateRight):
            self.accel += PADDLE_ACCEL if event.active else -PADDLE_ACCEL


class ClientPaddle(pb.Referenceable, EventListenerBase, DrawableObject):
    """Бита на клиенте"""
    def __init__(self, event_manager):
        """
        :type event_manager: events.EventManager
        """
        EventListenerBase.__init__(self, event_manager)
        self.pos = Vec2d()
        self.connected = False
        self.remote_ref = None

    def on_connect(self, remote_ref):
        self.remote_ref = remote_ref
        self.connected = True

    def remote_set_pos(self, x, y):
        self.pos = Vec2d(x, y)

    def render(self, renderer):
        """Рисует биту в ее текущей позиции
        :type renderer: render.RendererBase
        """
        if not self.connected:
            return
        renderer.draw_tex_rect(self.pos - PADDLE_EXTENTS, PADDLE_SIZE, 'paddle')

    def process_event(self, event):
        if not self.connected:
            return
        if isinstance(event, PaddleAccelerateLeft):
            self.remote_ref.callRemote('accelerate_left', event.active)
        elif isinstance(event, PaddleAccelerateRight):
            self.remote_ref.callRemote('accelerate_right', event.active)


class ServerPaddle(pb.Referenceable, CollidingRect):
    """Бита на сервере"""
    def __init__(self, pos, remote_ref):
        CollidingRect.__init__(self, pos, Vec2d(), Vec2d(), PADDLE_EXTENTS)
        self.remote_ref = remote_ref

    def remote_accelerate_left(self, active):
        self.accel += -PADDLE_ACCEL if active else PADDLE_ACCEL

    def remote_accelerate_right(self, active):
        self.accel += PADDLE_ACCEL if active else -PADDLE_ACCEL

    def set_params(self, pos, speed):
        CollidingObject.set_params(self, pos, speed)
        self.remote_ref.callRemote('set_pos', pos.x, pos.y)
