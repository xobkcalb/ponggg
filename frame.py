# -*- coding: utf-8 -*-

from __future__ import division, print_function
import logging
import pygame
import time
from twisted.internet import reactor, task

from events import EventListenerBase
from event_classes import FrameStartEvent, ExitGameEvent, FrameInfoEvent, DebugElapsedSlowdown, DebugElapsedSpeedup

'''ограничения по частоте фреймов (игра не увидит elapsed соответствующий
слишком большому или слишком маленькому значению FPS)'''
FPS_MIN, FPS_MAX = 10, 50

ELAPSED_SLOWDOWN = (1, 1.5, 2, 3, 5, 10, 25, 100)


class TickerBase(EventListenerBase):
    """Синглетон, который генерирует евенты начала фрейма"""
    def __init__(self, event_manager):
        """
        :type event_manager: events.EventManager
        """
        EventListenerBase.__init__(self, event_manager)
        self.elapsed_slowdown = 0
        self.tick_start = time.time()
        self.tick_end = self.tick_start

    def tick_frame(self):
        tick_start = time.time()
        true_elapsed = 1000*(tick_start - self.tick_start)
        last_busy_time = 1000*(self.tick_end - self.tick_start)

        elapsed = min(true_elapsed, 1000 / FPS_MIN)
        elapsed /= ELAPSED_SLOWDOWN[self.elapsed_slowdown]
        logging.debug("loop time: %.fms, busy time: %.fms, elapsed time: %.fms",
                      true_elapsed, last_busy_time, elapsed)
        self.event_manager.post_event(FrameInfoEvent('loop time', true_elapsed))
        self.event_manager.post_event(FrameInfoEvent('elapsed time', elapsed))
        self.event_manager.post_event(FrameInfoEvent('busy time', last_busy_time))
        self.event_manager.post_event(FrameInfoEvent('elapsed slowdown', ELAPSED_SLOWDOWN[self.elapsed_slowdown]))

        self.event_manager.post_event(FrameStartEvent(elapsed))

        self.tick_start = tick_start
        self.tick_end = time.time()

    def on_exit(self):
        raise NotImplementedError()

    def process_event(self, event):
        if isinstance(event, ExitGameEvent):
            self.on_exit()
        elif isinstance(event, DebugElapsedSlowdown):
            if self.elapsed_slowdown < len(ELAPSED_SLOWDOWN) - 1:
                self.elapsed_slowdown += 1
        elif isinstance(event, DebugElapsedSpeedup):
            if self.elapsed_slowdown > 0:
                self.elapsed_slowdown -= 1


# noinspection PyAbstractClass
class FrameTicker(TickerBase):
    def __init__(self, event_manager):
        """
        :type event_manager: events.EventManager
        """
        TickerBase.__init__(self, event_manager)
        self.running = True
        self.clock = pygame.time.Clock()
        self.clock.tick()  # Первый вызов до цикла чтобы первый get_time() работал правильно

    def run(self):
        while self.running:
            '''Основной цикл программы'''
            self.tick_frame()

            '''Ждем конца фрейма'''
            self.clock.tick(FPS_MAX)

    def on_exit(self):
        self.running = False


class TwistedTicker(TickerBase):
    def __init__(self, event_manager):
        """
        :type event_manager: events.EventManager
        """
        TickerBase.__init__(self, event_manager)
        self.task = task.LoopingCall(self.tick_frame)
        self.task.start(1/FPS_MAX)

    def on_exit(self):
        reactor.stop()
