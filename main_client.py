# -*- coding: utf-8 -*-

from __future__ import division, print_function
import logging
import pygame
from twisted.spread import pb
from twisted.internet import reactor
from debug import DebugInfo

from events import EventManager
from frame import TwistedTicker
from input import KeyboardHandler
from paddle import ClientPaddle
from render import SimpleRenderer
from render.textures import init_texlib

WIDTH, HEIGHT = 1200, 700  # Размеры экрана


def on_error(error):
    logging.error('Twisted error: %s', error)
    reactor.stop()


def init():
    """Инициализация программы"""
    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.INFO)

    pygame.init()
    pygame.display.set_caption('Ping-Pong Multi')
    pygame.display.set_mode((WIDTH, HEIGHT))

    init_texlib('data/textures')


def main():
    init()

    event_manager = EventManager()

    '''Порядок создания листенеров это порядок их вызова для обработки евента'''
    TwistedTicker(event_manager)
    KeyboardHandler(event_manager)
    renderer = SimpleRenderer(event_manager)
    debug_info = DebugInfo(event_manager)

    renderer.add_objects([debug_info])

    def on_connect(server_ref):
        logging.info('got server!')
        paddle = ClientPaddle(event_manager)
        server_ref.callRemote('connect', paddle).addCallback(paddle.on_connect)

        renderer.add_objects([paddle])

    factory = pb.PBClientFactory()
    reactor.connectTCP('localhost', 8321, factory)
    deferred_server = factory.getRootObject()
    deferred_server.addCallback(on_connect)
    deferred_server.addErrback(on_error)
    reactor.run()

if __name__ == '__main__':
    main()
