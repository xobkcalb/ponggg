# -*- coding: utf-8 -*-

from __future__ import division, print_function
from util import Vec2d


class CollidingObject:
    def __init__(self, pos, speed, accel, immovable=False):
        """
        :type pos: Vec2d
        :type speed: Vec2d
        :type accel: Vec2d
        :type immovable: bool
        """
        self.pos = pos
        self.speed = speed
        self.accel = accel
        self.immovable = immovable

    def set_params(self, pos, speed):
        self.pos = pos
        self.speed = speed


class CollidingCircle(CollidingObject):
    def __init__(self, pos, speed, accel, radius):
        """
        :type pos: Vec2d
        :type speed: Vec2d
        :type accel: Vec2d
        :type radius: float
        """
        CollidingObject.__init__(self, pos, speed, accel)
        self.radius = radius


class CollidingRect(CollidingObject):
    def __init__(self, pos, speed, accel, extents):
        """
        :type pos: Vec2d
        :type speed: Vec2d
        :type accel: Vec2d
        :type extents: Vec2d
        """
        CollidingObject.__init__(self, pos, speed, accel)
        self.extents = extents


class WorldBoundsRect(CollidingObject):
    def __init__(self, pos, extents):
        """
        :type pos: Vec2d
        :type extents: Vec2d
        """
        CollidingObject.__init__(self, pos, Vec2d(), Vec2d(), True)
        self.extents = extents
