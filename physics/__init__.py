# -*- coding: utf-8 -*-

from __future__ import division, print_function
import itertools
import time

from events import EventListenerBase
from event_classes import FrameStartEvent, FrameInfoEvent, FlipGravity
from physics.objects import WorldBoundsRect, CollidingCircle, CollidingRect
from util import Vec2d


class _ObjectSimulationStep:
    def __init__(self, obj):
        """
        :type obj: physics.objects.CollidingObject
        """
        self.obj = obj
        self.new_pos = obj.pos
        self.new_speed = obj.speed

    def move_step(self, elapsed, gravity, friction):
        if self.obj.immovable:
            return
        self.new_pos = self.obj.pos + self.obj.speed*elapsed + self.obj.accel*0.5*elapsed*elapsed
        self.new_speed = self.obj.speed + self.obj.accel*elapsed

        self.new_pos += gravity*0.5*elapsed*elapsed
        self.new_speed += gravity*elapsed

        self.new_speed *= 1 - friction

    def apply_step(self):
        if self.obj.immovable:
            return
        self.obj.set_params(self.new_pos, self.new_speed)


class _CollisionOverlap:
    def __init__(self, sim1, sim2, collisions):
        """
        :type sim1: _ObjectSimulationStep
        :type sim2: _ObjectSimulationStep
        :type collisions: list[Vec2d]
        """
        self.sim1 = sim1
        self.sim2 = sim2
        self.collisions = collisions


class PhysicsSimulator(EventListenerBase):
    """Синглетон, который обрабатывает физику"""
    def __init__(self, event_manager):
        """
        :type event_manager: events.EventManager
        """
        EventListenerBase.__init__(self, event_manager)
        self.objects = []
        """:type : list[physics.objects.CollidingObject]"""

        self.gravity = Vec2d(0, 0.003)  # pixels/ms^2
        self.friction = 0.005  # speed value decrease each second

    def add_objects(self, object_list):
        """
        :type object_list: collections.Iterable[physics.objects.CollidingObject]
        """
        self.objects.extend(object_list)

    @staticmethod
    def flip_vector(vector, flip_direction):
        """Отражает вектор vector от плоскости, заданной перпендикуляром flip_direction
        :type vector: Vec2d
        :type flip_direction: Vec2d
        """
        normal = flip_direction.vector_norm()
        # noinspection PyTypeChecker
        return vector - normal*2*Vec2d.dot(vector, normal)

    def try_collide(self, sim1, sim2):
        """
        :type sim1: _ObjectSimulationStep
        :type sim2: _ObjectSimulationStep
        """
        if isinstance(sim1.obj, WorldBoundsRect):
            return self.collide_bounds(sim1, sim2)
        if isinstance(sim2.obj, WorldBoundsRect):
            return self.collide_bounds(sim2, sim1)
        return None

    @staticmethod
    def collide_bounds(bounds_sim, obj_sim):
        """
        :type bounds_sim: _ObjectSimulationStep
        :type obj_sim: _ObjectSimulationStep
        """
        bounds = bounds_sim.obj
        assert isinstance(bounds, WorldBoundsRect)
        obj = obj_sim.obj
        new_pos = obj_sim.new_pos
        if isinstance(obj, CollidingCircle):
            # проверяем пересечение круга с вертикальными стенками
            x_hit = None
            radius = obj.radius
            if new_pos.x + radius >= bounds.pos.x + bounds.extents.x:
                x_hit = Vec2d(new_pos.x + radius, new_pos.y)
            elif new_pos.x - radius <= bounds.pos.x - bounds.extents.x:
                x_hit = Vec2d(new_pos.x - radius, new_pos.y)

            # проверяем пересечение круга с горизонтальными стенками
            y_hit = None
            if new_pos.y + radius >= bounds.pos.y + bounds.extents.y:
                y_hit = Vec2d(new_pos.x, new_pos.y + radius)
            elif new_pos.y - radius <= bounds.pos.y - bounds.extents.y:
                y_hit = Vec2d(new_pos.x, new_pos.y - radius)

            if x_hit is None and y_hit is None:
                return None

            return _CollisionOverlap(bounds_sim, obj_sim, [hit for hit in x_hit, y_hit if hit is not None])
        elif isinstance(obj, CollidingRect):
            # проверяем пересечение прямоугольника с вертикальными стенками
            x_hit = None
            extents = obj.extents
            if new_pos.x + extents.x >= bounds.pos.x + bounds.extents.x:
                x_hit = Vec2d(new_pos.x + extents.x, new_pos.y)
            elif new_pos.x - extents.x <= bounds.pos.x - bounds.extents.x:
                x_hit = Vec2d(new_pos.x - extents.x, new_pos.y)

            # проверяем пересечение прямоугольника с горизонтальными стенками
            y_hit = None
            if new_pos.y + extents.y >= bounds.pos.y + bounds.extents.y:
                y_hit = Vec2d(new_pos.x, new_pos.y + extents.y)
            elif new_pos.y - extents.y <= bounds.pos.y - bounds.extents.y:
                y_hit = Vec2d(new_pos.x, new_pos.y - extents.y)

            if x_hit is None and y_hit is None:
                return None

            return _CollisionOverlap(bounds_sim, obj_sim, [hit for hit in x_hit, y_hit if hit is not None])
        else:
            return None

    def process_event(self, event):
        if isinstance(event, FlipGravity):
            self.gravity = -self.gravity
        elif isinstance(event, FrameStartEvent):
            start_time = time.time()
            elapsed = event.elapsed

            '''пробуем подвинуть объекты'''
            simulated = [_ObjectSimulationStep(o) for o in self.objects]
            for sim in simulated:
                sim.move_step(elapsed, self.gravity, self.friction)

            '''коллижен'''
            for sim1, sim2 in itertools.combinations(simulated, 2):
                collision = self.try_collide(sim1, sim2)
                if not collision:
                    continue
                for sim in sim1, sim2:
                    for collision_pos in collision.collisions:
                        push_direction = sim.new_pos - collision_pos
                        # отражаем скорость если объект двигается в направлении точки слоткновения
                        if Vec2d.dot(push_direction, sim.new_speed) < 0:
                            sim.new_speed = self.flip_vector(sim.new_speed, push_direction)

            '''применяем новые координаты и скорости'''
            for sim in simulated:
                sim.apply_step()

            end_time = time.time()
            spend_msec = int(1000*(end_time - start_time))
            self.event_manager.post_event(FrameInfoEvent('physics time', spend_msec))
