# -*- coding: utf-8 -*-
from __future__ import division, print_function
import numbers
import math


class Vec2d(object):
    """Иммутабельный двумерный вектор.

    Доступ к полям:
      * по имени: ``a.x``, ``a.y``
      * по индексу: ``a[0]``, ``a[1]``
      * итерация по компонентам: ``for x in a:``

    Поддерживаемые операции:
      * ``a==b``, ``a!=b`` -- сравнение на равенство
      * ``+a``, ``-a`` -- унарный плюс и минус
      * ``a+b``, ``a-b`` -- сложение и вычитание векторов
      * ``a*x``, ``a/x`` -- умножение и деление на скаляр
      * ``len(a)`` -- размерность вектора (всегда возвращает 2 для совместимости с итерированием по компонентам)
      * ``abs(a)`` -- покомпонентный модуль
      * ``Vec2d.min(a, b)``, ``Vec2d.max(a, b)`` -- покомпонентный минимум/максимум
      * ``Vec2d.dot(a, b)`` -- скалярное произведение векторов
      * ``a.vector_size()`` -- длина вектора
      * ``a.vector_norm()`` -- возвращает нормированный вектор длиной 1 (или ``Vec(0, 0)`` для нулевого вектора)
      * ``a.component_mul(b)`` -- покомпонентное умножение
      * ``a.int_cast()`` -- конвертация компонентов в ``int``
      * ``a.component_apply(fun)`` -- поэлементное применение функции к вектору
    """
    def __init__(self, x=0, y=0):
        """
        :type x: int | float
        :type y: int | float
        """
        self.data = (x, y)

    @property
    def x(self):
        return self.data[0]

    @property
    def y(self):
        return self.data[1]

    def __iter__(self):
        return self.data.__iter__()

    def __getitem__(self, item):
        """
        :type item: int
        """
        return self.data.__getitem__(item)

    def __pos__(self):
        return self

    def __neg__(self):
        return self.component_apply(lambda x: -x)

    def __abs__(self):
        return self.component_apply(abs)

    def __add__(self, other):
        """
        :type other: Vec2d
        """
        return Vec2d(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        """
        :type other: Vec2d
        """
        return self.__add__(-other)

    def __mul__(self, other):
        """
        :type other: int | float
        """
        self.__assert_scalar(other)
        return self.component_apply(lambda x: x * other)

    def __truediv__(self, other):
        """
        :type other: int | float
        """
        self.__assert_scalar(other)
        return self.component_apply(lambda x: x / other)

    def __eq__(self, other):
        """
        :type other: Vec2d
        """
        return self.data == other.data

    def __ne__(self, other):
        """
        :type other: Vec2d
        """
        return not self.__eq__(other)

    def __len__(self):
        return self.data.__len__()

    def __repr__(self):
        return '(x: %s, y: %s)' % self.data

    @staticmethod
    def __assert_scalar(value):
        if not isinstance(value, numbers.Real):
            raise TypeError('%s needs to be a scalar' % type(value))

    @staticmethod
    def min(v1, v2):
        """
        :type v1: Vec2d
        :type v2: Vec2d
        """
        return Vec2d(min(v1.x, v2.x), min(v1.y, v2.y))

    @staticmethod
    def max(v1, v2):
        """
        :type v1: Vec2d
        :type v2: Vec2d
        """
        return Vec2d(max(v1.x, v2.x), max(v1.y, v2.y))

    @staticmethod
    def dot(v1, v2):
        """
        :type v1: Vec2d
        :type v2: Vec2d
        """
        return v1.x*v2.x + v1.y*v2.y

    def component_mul(self, other):
        """
        :type other: Vec2d
        """
        return Vec2d(self.x*other.x, self.y*other.y)

    def int_cast(self):
        return self.component_apply(int)

    def vector_size(self):
        return math.sqrt(self.x*self.x + self.y*self.y)

    def vector_norm(self):
        size = self.vector_size()
        if size == 0:
            size = 1
        return self/size

    def component_apply(self, fun):
        """
        :type fun: (int | float) -> int | float
        """
        return Vec2d(fun(self.x), fun(self.y))
