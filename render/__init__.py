# -*- coding: utf-8 -*-

from __future__ import division, print_function
import logging
import pygame
import pygame.gfxdraw  # нужен явный импорт, иначе по дефолту в pygame не появляется gfxdraw

from events import EventListenerBase
from event_classes import FrameStartEvent, FlipGravity
from render.textures import get_texture
from util import Vec2d

BG_COLOR = 50, 40, 30  # Цвет фона


# noinspection PyClassHasNoInit
class DrawableObject:
    def render(self, renderer):
        """
        :type renderer: RendererBase
        """
        raise NotImplementedError()


class RendererBase(EventListenerBase):
    def __init__(self, event_manager):
        """
        :type event_manager: events.EventManager
        """
        EventListenerBase.__init__(self, event_manager)
        self.objects = []

    def add_objects(self, objects):
        """
        :type objects: collections.Iterable[DrawableObject]
        """
        self.objects.extend(objects)

    def process_event(self, event):
        if isinstance(event, FrameStartEvent):
            self.begin_render()

            '''Прорисовка объектов в новой позиции'''
            for obj in self.objects:
                obj.render(self)

            self.end_render()

    def begin_render(self):
        raise NotImplementedError()

    def end_render(self):
        raise NotImplementedError()

    def draw_circle(self, color, pos, radius):
        """
        :type color: (int, int, int)
        :type pos: util.Vec2d
        :type radius: int
        """
        raise NotImplementedError()

    def draw_rect(self, color, pos, size):
        """
        :type color: (int, int, int)
        :type pos: util.Vec2d
        :type size: util.Vec2d
        """
        raise NotImplementedError()

    def draw_tex_rect(self, pos, size, tex_name):
        """
        :type pos: util.Vec2d
        :type size: util.Vec2d
        :type tex_name: str
        """
        raise NotImplementedError()

    def draw_text(self, color, pos, text):
        """
        :type color: (int, int, int)
        :type pos: util.Vec2d
        :type text: str
        """
        raise NotImplementedError()


# noinspection PyAbstractClass
class SimpleRenderer(RendererBase):
    """Синглетон, который рисует графику при помощи pygame.draw.*"""
    def __init__(self, event_manager):
        """
        :type event_manager: events.EventManager
        """
        RendererBase.__init__(self, event_manager)
        self.font = pygame.font.SysFont("monospace", 15)
        self.bg_color = BG_COLOR

    def process_event(self, event):
        if isinstance(event, FlipGravity):
            self.bg_color = tuple(255 - c for c in self.bg_color)
        else:
            RendererBase.process_event(self, event)

    def begin_render(self):
        pygame.display.get_surface().fill(self.bg_color)

    def end_render(self):
        pygame.display.update()  # Обновление экрана

    def draw_circle(self, color, pos, radius):
        screen = pygame.display.get_surface()
        pygame.draw.circle(screen, color, pos.int_cast(), radius)

    def draw_rect(self, color, pos, size):
        screen = pygame.display.get_surface()
        pygame.draw.rect(screen, color, pygame.Rect(pos.int_cast(), size))

    def draw_tex_rect(self, pos, size, tex_name):
        screen = pygame.display.get_surface()
        size -= Vec2d(0, 1)  # for whatever motherfucking reason pygame renderer draws textures one pixel higher
        polygon = [(pos.x, pos.y), (pos.x + size.x, pos.y), (pos.x + size.x, pos.y + size.y), (pos.x, pos.y + size.y)]
        texture = get_texture(tex_name)
        pos = pos.int_cast()
        pygame.gfxdraw.textured_polygon(screen, polygon, texture, pos.x, -pos.y)

    def draw_text(self, color, pos, text):
        screen = pygame.display.get_surface()
        label = self.font.render(text, True, color)
        screen.blit(label, pos)


# noinspection PyAbstractClass
class LoggingRenderer(RendererBase):
    def __init__(self, event_manager, level):
        """
        :type event_manager: events.EventManager
        :type level: int
        """
        RendererBase.__init__(self, event_manager)
        self.logger = logging.getLogger('render')
        self.level = level
        self.frame_counter = 0

    def begin_render(self):
        self.frame_counter += 1

    def end_render(self):
        pass

    def draw_circle(self, color, pos, radius):
        pass

    def draw_rect(self, color, pos, size):
        pass

    def draw_tex_rect(self, pos, size, tex_name):
        pass

    def draw_text(self, color, pos, text):
        self.logger.log(self.level, 'frame %d: %s', self.frame_counter, text)
