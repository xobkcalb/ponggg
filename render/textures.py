# -*- coding: utf-8 -*-

from __future__ import division, print_function
import os
import pygame

_SUPPORTED_EXT = ('.png',)


class _TexLib:
    def __init__(self, tex_dir):
        self.tex_dir = os.path.normpath(tex_dir)
        self.textures = {}
        for fname in os.listdir(self.tex_dir):
            name, ext = os.path.splitext(fname)
            if ext.lower() not in _SUPPORTED_EXT:
                continue
            tex = pygame.image.load(os.path.join(self.tex_dir, fname))
            self.textures[name.lower()] = tex

    def get_texture(self, name):
        return self.textures.get(name.lower())


_texlib = None


def init_texlib(tex_dir):
    global _texlib
    _texlib = _TexLib(tex_dir)


def get_texture(name):
    if not _texlib:
        return None
    return _texlib.get_texture(name)
