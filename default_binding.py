# -*- coding: utf-8 -*-

from __future__ import division, print_function
import pygame

from event_classes import PaddleAccelerateLeft, PaddleAccelerateRight, FlipGravity, DebugElapsedSlowdown, \
    DebugElapsedSpeedup, KeyboardEvent, MenuActionShowMenu
from input import KeyboardBinding, Keybind


def register_keyboard_binding(event_manager):
    """
    :type event_manager: events.EventManager
    """
    binding = KeyboardBinding(event_manager, [
        # Меню по эскейпу
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_ESCAPE, MenuActionShowMenu),

        # Управление битой: <- двигает влево, -> двигает вправо
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_LEFT, PaddleAccelerateLeft, True),
        Keybind(KeyboardEvent.KEY_RELEASED, pygame.K_LEFT, PaddleAccelerateLeft, False),
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_RIGHT, PaddleAccelerateRight, True),
        Keybind(KeyboardEvent.KEY_RELEASED, pygame.K_RIGHT, PaddleAccelerateRight, False),

        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_SPACE, FlipGravity),
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_KP_PLUS, DebugElapsedSlowdown),
        Keybind(KeyboardEvent.KEY_PRESSED,  pygame.K_KP_MINUS, DebugElapsedSpeedup),
    ])
    event_manager.register_listener(binding)
